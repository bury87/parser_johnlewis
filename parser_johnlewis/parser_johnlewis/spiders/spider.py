# coding: utf8
import scrapy
import re
import json
import csv
import sys
import logging
from parser_johnlewis.items import ParserJohnlewisItem

class ParserJohnlewisSpider(scrapy.Spider):

    name = "johnlewis"

    def start_requests(self):
        url = 'https://www.johnlewis.com/'
        yield scrapy.Request(url=url, callback=self.parse_category)


    def parse_category(self, response):
        urls = response.xpath(".//div[@class='nn-flyout']//ul//a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse_sub_sub_category)

    def parse_sub_category(self, response):
        if response.headers.get('Content-Type') != 'application/pdf':
            urls = response.xpath(".//div[@class='products']//a[@class='product-link product']/@href").extract()
            for url in urls:
                yield scrapy.Request(url=response.urljoin(url), callback=self.parse_item)
            pagination = response.xpath(
                ".//div[@class='pagination']/ul/li[@class='next']/a[not(@class)]/@href").extract_first(default=None)
            if pagination is not None:
                yield scrapy.Request(url=response.urljoin(pagination), callback=self.parse_sub_category)

    def parse_sub_sub_category(self, response):
        if response.headers.get('Content-Type') != 'application/pdf':
            sub_cat = response.xpath(".//div[@class='col-3 first lt-nav']//a/@href").extract()
            if len(sub_cat)>0:
                for sbc in sub_cat:
                    yield scrapy.Request(url=response.urljoin(sbc), callback=self.parse_sub_sub_category)
            else:
                pagination = response.xpath(".//div[@class='pagination']/ul/li[@class='next']/a[not(@class)]/@href").extract_first(default=None)
                if pagination is not None:
                    yield scrapy.Request(url=response.urljoin(pagination), callback=self.parse_sub_category)
                urls = response.xpath(".//div[@class='products']//a[@class='product-link product']/@href").extract()
                for url in urls:
                    yield scrapy.Request(url=response.urljoin(url), callback=self.parse_item)


    def parse_item(self, response):
        product = ParserJohnlewisItem()
        product['url'] = response.url
        product['id'] = response.xpath(".//div[@itemprop='productID']/@content").extract_first(default='')
        product['img'] = response.xpath(".//img[@itemprop='image']/@src").extract_first(default='')
        product['produckt_group'] = response.xpath(".//div[@id='breadcrumbs']//ol/li[position()=2]/a/text()").extract_first(
            default='')
        product['name'] = response.xpath(".//h1/span[@itemprop='name']/text()").extract_first(default='').replace('"','&#34;').replace(',','&#44;').strip()
        try:
            price = response.xpath(
                ".//span[@itemprop='price']/text()").extract_first(default=None)
            if price is None:
                price = response.xpath(
                    ".//div[@id='prod-price']/p[@class='price']/strong/text()").extract_first(default=None)
            if price is None:
                price = response.xpath(
                        ".//strong[@class='price']/text()").extract_first(default="0")
            price = price.split("-")[0].strip()
            price = re.sub(r'[a-zA-Z ]', '', price)
            product['price'] = float(price.replace(u'£','').replace(u',',''))
            if product['id'] != '':
                yield product
        except Exception as e:
            print e.message
            logging.error("Price error in url: " + product['url'])



















